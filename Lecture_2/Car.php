<?php
class Person{
    public $name;
    public $lastname;
}

class Car
{
    public $model = "ML";
    public $date = "21.09.2021";
    private $colour = "Black";
    private $price = 0;
    protected $manufacturer = "Mersedes";
    const owner = "Beso Tabatadze";

    public function get_colour(){
//        echo $this->price;
        return $this->colour;
    }
}

class ML extends Car{
    public function print_model(){
        echo $this->model;
        echo "<br>";
        echo $this->manufacturer;
        echo "<br>";
    }
}

$car_1 = new Car();
echo $car_1->get_colour();
echo "<hr>";
echo $car_1->model;
$car_1->model = "GL";

$ml_1 = new ML();
$ml_1->print_model();

interface actions{
    public function honking();
    public function going($speed);
}

interface owner{
//
    public function get_name();
    public function get_id();
}

abstract class Owner1{

}

final class Owner2{

}

$o = new Owner2();

class Owner3 extends Owner1{

}


class Car1 implements actions, owner {

    public function honking()
    {
        return "Car is honking";
    }

    public function going($speed)
    {
        return "Car speed equal ".$speed;
    }

    public function get_name()
    {

    }

    public function get_id()
    {
        // TODO: Implement get_id() method.
    }
}





