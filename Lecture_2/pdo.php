<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "webapp2021";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
    $sql = "INSERT INTO users (name, lastname, age) VALUES ('John', 'Doe', 20)";
    $conn->exec($sql);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
