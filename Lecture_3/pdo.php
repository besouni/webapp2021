<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "webapp2021";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
//    $sql = "INSERT INTO users (name, lastname, age) VALUES ('John', 'Doe', 20)";
//    $conn->exec($sql);
    $name = "Beso";
    $lastname = "Tabatadze";
    $age = 37;
    $sql1 = "INSERT INTO users (name, lastname, age) VALUES (:name, :lastname, :age)";
    $stmt = $conn->prepare($sql1);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':lastname', $lastname);
    $stmt->bindParam(':age', $age);
    $stmt->execute();

} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
