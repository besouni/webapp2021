<?php
//    include "Car.php";

trait car_functions{
    public function test1(){
        echo "<h1>This is test1 trait</h1>";
    }
    public function test2(){
        echo "<h1>This is test2 trait</h1>";
    }
    public function test3($price){
        echo "<h1>This is $price</h1>";
    }
}

class Car{
    use car_functions;
    public $model = "ML";
    public $date = "21.09.2021";
    private $colour = "Black";
    protected $manufacturer = "Mersedes";
    const owner = "Beso Tabatadze";
    private $price = 0;

    function __construct($price=7000){
        echo "<b>Construct</b>";
        echo "<hr>";
        $this->price = $price;
    }

    public function test_trait(){
        $this->test1();
        $this->test3($this->price);
    }

    public function get_colour(){
        return $this->colour;
    }

    function get_manufacturer(){
        return$this->manufacturer;
    }

    function get_this_array(){
        return (array) $this;
    }

    public function get_price(){
        return $this->price;
    }
}


    $car_1 = new Car(5000);
    $car_2 = new Car(6000);
    $car_3 =  new Car();
    $car_2->model = "GL";
    echo $car_1->model;
    echo "<hr>";
    echo $car_2->model;
    echo "<hr>";
//    echo $car_2->colour;
    echo $car_1->get_colour();
    echo "<hr>";
    echo $car_1->get_manufacturer();
    echo "<hr>";
    echo $car_1::owner;
    echo "<hr>";
    var_dump($car_1->get_this_array());
    echo "<hr>";
    echo $car_1->get_price();
    echo "<hr>";
    echo $car_2->get_price();
    echo "<hr>";
    echo $car_3->get_price();
    echo "<hr>";
    echo $car_3->test1();
    echo "<hr>";
    echo $car_3->test_trait();
?>